# ree

Output the last part of files, just like `tail`.
It will stop if a file is deleted.
If a file is moved, it will keep tracking it.

Run `ree --help` to view all options.
