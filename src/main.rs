use clap::{Arg, ArgAction, Command};
use inotify::{EventMask, Inotify, WatchMask};
use serde_json::{Map, Result as SerdeResult, Value as SerdeValue};
use std::collections::VecDeque;
use std::fs::File;
use std::io::{self};
use std::io::{BufRead, BufReader};
use std::path::Path;
use std::{process, thread};

#[derive(Debug, Copy, Clone)]
struct Options {
    follow: bool,
    nb_lines: isize,
    pretty_print_json: bool,
    merge_array: bool,
}

struct FileLinesBuffer {
    file_name: String,
    lines: VecDeque<String>,
    lines_skipped: usize,
    options: Options,
}

impl FileLinesBuffer {
    fn new(file_name: String, options: Options) -> Self {
        Self {
            file_name,
            options,
            lines: VecDeque::new(),
            lines_skipped: 0,
        }
    }

    fn add_line(&mut self, line: String) {
        self.lines.push_back(line);

        if self.options.nb_lines >= 0 {
            if self.lines.len() > self.options.nb_lines as usize {
                self.lines.pop_front();
            }
        } else {
            if self.lines_skipped < -self.options.nb_lines as usize {
                self.lines_skipped += 1;
                self.lines.pop_front();
            }
        }
    }

    fn print(self) {
        for line in self.lines {
            print_file_line(self.file_name.clone(), line.as_str(), self.options);
        }
    }
}

fn main() {
    let matches = Command::new("ree")
        .about("Output the last part of files")
        .arg(
            Arg::new("file_names")
                .help("Input files")
                .num_args(1..)
                .required(true),
        )
        .arg(
            Arg::new("follow")
                .help("Append data to output as the file grows")
                .short('f')
                .long("follow")
                .required(false)
                .action(ArgAction::SetTrue),
        )
        .arg(
            Arg::new("nb_lines")
                .help("output the last NUM lines instead of the default 10. Pass -X or -X to skip the first X lines of the file.")
                .short('n')
                .long("lines")
                .required(false)
                .allow_negative_numbers(true)
                .value_parser(clap::value_parser!(isize))
                .default_value("10"),
        )
        .arg(
            Arg::new("pretty_print_json")
                .help("Parse the JSON and pretty print it on multiple lines. Each line must contain a valid JSON object.")
                .long("pretty-json")
                .required(false)
                .action(ArgAction::SetTrue)
        )
        .arg(
            Arg::new("merge_array")
                .help("If arrays are found in the INPUT, they will be joined into a string. Each element will be separated by a space. This only has an effect if --pretty-json is passed!")
                .long("merge-array")
                .required(false)
                .action(ArgAction::SetTrue)
        )
        .get_matches();

    let mut thread_handles = vec![];
    let lines = *matches
        .get_one("nb_lines")
        .expect("Failed to parse the number of lines");
    if let Some(file_names) = matches.get_many::<String>("file_names") {
        for file_name in file_names {
            let options = Options {
                follow: matches.get_flag("follow"),
                nb_lines: lines,
                pretty_print_json: matches.get_flag("pretty_print_json"),
                merge_array: matches.get_flag("merge_array"),
            };
            let fname = file_name.clone();
            let handle = thread::spawn(move || {
                return process_arg(fname, options);
            });
            thread_handles.push(handle);
        }
    }

    let mut exit_code = 0;
    for handle in thread_handles {
        match handle.join() {
            Ok(result) => match result {
                Ok(()) => {}
                Err(()) => {
                    exit_code = 1;
                }
            },
            Err(_) => {
                eprintln!("An error occurred while joining the thread.");
                exit_code = 2;
            }
        }
    }

    process::exit(exit_code);
}

fn process_arg(file_name: String, options: Options) -> Result<(), ()> {
    return if file_name == "-" {
        process_stdin(options)
    } else {
        process_file(file_name, options)
    };
}

fn process_stdin(options: Options) -> Result<(), ()> {
    let mut must_continue = true;
    while must_continue {
        let mut line = String::new();
        match io::stdin().read_line(&mut line) {
            Ok(read_length) => {
                must_continue = read_length != 0;
                print_file_line(String::from("<stdin>"), &line, options);
            }
            Err(error) => {
                eprintln!("Error while reading line: {}", error);
                return Err(());
            }
        }
    }

    return Ok(());
}

fn process_file(file_name: String, options: Options) -> Result<(), ()> {
    let file = open_file(file_name.clone())?;
    let other_handle = file.try_clone().unwrap();
    let mut reader = BufReader::new(file);
    let mut must_continue = true;
    let mut file_lines_buffer = FileLinesBuffer::new(file_name.clone(), options);

    while must_continue {
        let (read_size, line) = read_line(&mut reader)?;
        must_continue = read_size > 0;
        if must_continue {
            file_lines_buffer.add_line(line);
        }
    }

    file_lines_buffer.print();

    if options.follow {
        return watch_file(file_name.clone(), other_handle, options);
    }

    return Ok(());
}

fn open_file(file_name: String) -> Result<File, ()> {
    return match File::open(file_name.clone()) {
        Ok(file) => Ok(file),
        Err(e) => {
            eprintln!("Error opening file {}: {}", file_name, e);
            Err(())
        }
    };
}

fn read_line(reader: &mut BufReader<File>) -> Result<(usize, String), ()> {
    let mut line = String::new();
    return match reader.read_line(&mut line) {
        Ok(read_size) => Ok((read_size, line)),
        Err(e) => {
            eprintln!("Error while reading line: {}", e);
            Err(())
        }
    };
}

fn watch_file(file_name: String, f: File, options: Options) -> Result<(), ()> {
    let mut inotify = Inotify::init().expect("Failed to initialize inotify");
    inotify
        .watches()
        // Since we have an opened handle to the file, it won’t be deleted. So we must check the
        // ATTRIB event and check whether the file still exists.
        .add(file_name.clone(), WatchMask::MODIFY | WatchMask::ATTRIB)
        .expect("Failed to watch");
    let mut file_reader = BufReader::new(f);

    loop {
        let events = read_inotify_event(&mut inotify)?;
        if was_file_modified(file_name.clone(), events)? {
            let line = read_line(&mut file_reader)?.1;
            print_file_line(file_name.clone(), &line, options);
        }
    }
}

fn read_inotify_event(inotify: &mut Inotify) -> Result<Vec<EventMask>, ()> {
    let mut events_buffer = [0u8; 4096];
    return match inotify.read_events_blocking(&mut events_buffer) {
        Ok(events) => Ok(events.into_iter().map(|event| event.mask).collect()),
        Err(e) => {
            eprintln!("Failed to read inotify events: {}", e);
            Err(())
        }
    };
}

fn was_file_modified(file_name: String, events: Vec<EventMask>) -> Result<bool, ()> {
    for event in events {
        if event.contains(EventMask::ATTRIB) && !file_exists(file_name.clone()) {
            eprintln!("File {} was deleting. Stopping.", file_name);
            return Err(());
        }
        if event.contains(EventMask::MODIFY) {
            return Ok(true);
        }
    }

    return Ok(false);
}

fn file_exists(file_path: String) -> bool {
    let path = Path::new(file_path.as_str());
    path.is_file()
}

fn print_file_line(file_name: String, line: &str, options: Options) {
    let line_to_print = if options.pretty_print_json {
        pretty_print_json(line, options.merge_array)
    } else {
        String::from(line)
    };
    println!("[{}] {}", file_name, line_to_print.trim());
}

fn pretty_print_json(line: &str, merge_arrays: bool) -> String {
    if let Some(decoded_object) = get_json_object(line) {
        let mut pretty_json = String::from("\n");
        for key in decoded_object.keys() {
            pretty_json.push_str(&format!(
                "\x1b[1m{}\x1b[0m: {}\n",
                key,
                pretty_print_json_value(&decoded_object[key], merge_arrays)
            ));
        }
        return pretty_json.to_string();
    }

    return String::from(line);
}

fn get_json_object(line: &str) -> Option<Map<String, SerdeValue>> {
    let decoded_result: SerdeResult<SerdeValue> = serde_json::from_str(line);

    if let Ok(decoded_value) = decoded_result {
        if let Some(decoded_object) = decoded_value.as_object() {
            return Some(decoded_object.clone());
        }
    }

    return None;
}

fn pretty_print_json_value(value: &SerdeValue, merge_arrays: bool) -> String {
    if !merge_arrays {
        return value.to_string();
    }

    return match value.as_array() {
        Some(array) => {
            let merged_values = array
                .iter()
                .map(|array_value| array_value.to_string())
                .collect::<Vec<String>>()
                .join(" ")
                .replace("\" \"", " ")
                .replace("\\n", "\n\t");
            return String::from("\n\t") + &merged_values;
        }
        None => value.to_string(),
    };
}
